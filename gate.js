class Gate{
  constructor(name, ins, outs, steps, output){
    this.name = name;
    this.ins = ins;
    this.outs = outs;
    this.steps = steps;
    this.output = output;
    /*
    steps[n]: {gate: x, args: y, ix: z}
    x being the gate function
    y being an array with information on where to find each of the arguments
    in the way specified below:
      a negative integer (-1 is the most elegant option) - the argument is in the input_arguments[] array
      a natural integer - the argument is a result of a different gate located in steps[], the gate's index is exactly this natural integer
      z being an array with information on where to find each of the arguments (now specifying indexes of the arrays specified by y)

    Examples
    Let's assume the output array of gate n is stored in outcomes[n].
    If y=[5] and z=[0] our gate has one argument and it should be retrieved from output[5][0]
    If y=[-1,2] and z[4,1] there are two arguments:
      Arg 0. should be retireved from input_arguments[4]
      Arg 1. should be retrieved from output[2][1]
    */

  }
  ev(input_arguments){
    var debug = false;
    var steps = this.steps;
    console.log(steps.length);
    var outcomes = new Array(steps.length);
    for (var i=0;i<steps.length;++i){
      outcomes[i] = new Array(steps[i].gate.outs);
      outcomes[i].map(a => -1);
    }
    var steps_stack = []; //steps are pushed here whenever they cannot be resolved due to unfinished computing (outcomes containing undefined values)
    var completed_steps = []; //holds indexes of completed steps
    var j=0;
    while (j<steps.length){
      if (steps_stack.length === 0){
        var i = j;
      }
      else{
        var i = steps_stack.pop();
        j--;
      }
        if (debug){
          console.log("Starting iteration with i="+i+" and j="+j);
          console.log("steps_stack now has " + steps_stack.length + " elements.");
          console.log("We'll be taking a look at the following step:");
          console.log(steps[i]);
        }
        var gate = steps[i].gate;
        var args = steps[i].args;
        var ix = steps[i].ix;
        var argus = [];

        var quitGettingArguments = false;
        args.forEach(function(arg, index){
          if (quitGettingArguments) return;
          if (arg<0) {
            argus.push(input_arguments[ix[index]]);
            return;
          }
          var argu = outcomes[arg][ix[index]];
          if (argu > -1){//if the argument has been calculated
            if (debug) console.log("outcomes["+arg+"]["+ix[index]+"] has been calculated and it's value is " + argu);
            argus.push(argu);
            return;
          }
          if (debug) console.log("outcomes["+arg+"]["+ix[index]+"] has not been calculated yet");
          if (debug) console.log("Adding this step and step " + arg + " to the stack.");
          steps_stack.push(i);
          steps_stack.push(arg);
          j++; //we want j to point at the next step after we finish the stack. the loop won't change it because it's constantly being incremented and decremeneted
          quitGettingArguments = true;
        });
        if (quitGettingArguments) continue;

        if (debug) console.log("running " + gate.name);
        if (debug) console.log(gate);
        outcomes[i] = gate.ev.call(gate,argus);
        if (debug) console.log(gate.name + " was given the arguments " + argus + " and returned " + outcomes[i]);
        j++;
    }

    var toReturn = [];
    this.output.forEach(function(bit){
      console.log(bit);
      console.log("args: " + bit.args);
      console.log("ix: " + bit.ix);
      toReturn.push(outcomes[bit.args][bit.ix]);
    });
    return toReturn;
  }

  truth(){
    var possibilities = binaryPermutation(this.ins);
    var self = this;
    var outputs = [];
    possibilities.forEach(function(possibility){
      outputs.push({input: possibility, output: self.ev(possibility)});
    });
    console.table(outputs);
  }
}

var AND = new Gate("AND", 2, 1);
AND.ev = function ([x,y]){
  return [x && y];
}
var NOT = new Gate("NOT", 1, 1);
NOT.ev = function ([x]){
  return [!x];
}

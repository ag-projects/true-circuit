var and = document.getElementById('and_gate');
var not = document.getElementById('not_gate');
var add = document.getElementById('add');
var wire = document.getElementById('wire');
var in_gate = document.getElementById('in_gate');
var out_gate = document.getElementById('out_gate');
var assemble = document.getElementById('assemble');
class GuiElement{
  constructor(image, type, x, y, remaining=0, delay=0){
    this.image = image;
    this.x = x;
    this.y = y;
    this.type = type;
    if (this.type === "menu"){
      this.width = 50;
      this.height = 60;
      this.border = 2;
    }
    this.animation = {
      remaining: remaining,
      delay: delay
    }
    this.io = [];
    this.r = 2;
    if (this.type === "and_gate") {
	this.io = [{x: 6, y: 12, color: "#000000"}, {x: 6, y:34, color: "#000000"}, {x:39, y:23, color: "#000000"}];
	this.gate = AND;
	this.cover.xoff = 7;
	this.cover.yoff = 6;
	this.cover.width = 35;
	this.cover.height = 38;
    }
    else if (this.type === "not_gate"){
      this.io = [{x: 6, y: 23, color: "#000000"}, {x:44, y:23, color: "#000000"}];
	this.gate = NOT;
	this.cover.xoff = 7;
	this.cover.yoff = 5;
	this.cover.width = 40;
	this.cover.height = 40;
    }
    else if (this.type === "in_gate") this.io = [{x:41, y:23, color: "#000000"}];
    else if (this.type === "out_gate") this.io = [{x:-1, y:22, color: "#000000"}];
      this.cover = {xoff: undefined, yoff: undefined, width: undefined, height: undefined};
    this.circles = []; //an array to hold circle centers locations
    if (this.type === "in_gate") {
      this.ix = ++max_in_index;
	this.ixText = numWithZeros(this.ix);
	this.cover.xoff = 0;
	this.cover.yoff = 4;
	this.cover.width = 45;
	this.cover.height = 38;
    }
    else if (this.type === "out_gate"){
	this.ix = ++max_out_index;
	this.ixText = numWithZeros(this.ix);
	this.cover.xoff = 0;
	this.cover.yoff = 4;
	this.cover.width = 45;
	this.cover.height = 38;
    }
  }
  show(){
    if (this.animation.delay>0) { this.animation.delay--; return; }
    if (this.animation.delay == 0 && this.animation.remaining > 0) { this.x++; this.animation.remaining--; }
    if (this.image !== "custom"){
      ctx.drawImage(this.image,this.x,this.y);
    }
    else {
      ctx.lineWidth = this.border;
      ctx.strokeStyle = "black";
      ctx.strokeRect(this.x-this.border, this.y-this.border, this.width + 2*this.border, this.height + 2*this.border);
      ctx.fillStyle = "gray";
      ctx.fillRect(this.x, this.y, this.width, this.height);
    }
    //console.log(this.type);
    var radius = 2, x = this.x, y = this.y;
    var circles = [];
    var self = this;
    this.io.forEach(function(io){
      ctx.strokeStyle = io.color;
      ctx.lineWidth = 4;
      ctx.beginPath();
      ctx.arc(x + io.x + radius, y + io.y + radius, radius, 0, 2*Math.PI);
      circles.push({x: x + io.x + radius, y: y + io.y + radius});
      ctx.stroke();
    });
    this.circles = circles;
    if (this.type === "in_gate" || this.type === "out_gate") {
      ctx.font = '12px monospace';
      ctx.fillStyle = "black";
      ctx.fillText(this.ixText, this.x + 11, this.y + 36);
    }
  }
  covers(mX,mY){
    if (this.image === "custom") {
      //console.log("1: " + (mX>this.x)+" 2: "+(mX<this.x+this.width)+" 3: "+(mY>this.y)+" 4: "+(mY<this.y+this.height));
      return ((mX>this.x-this.border) && (mX<this.x+this.width+this.border) && (mY>this.y-this.border) && (mY<this.y+this.height+this.border));
    }
      console.log(this.cover);
      let {offx, offy, width, height} = this.cover;
      console.log(this.x + offx,this.x+offx+width);
      return ((mX > this.x + offx ) && (mX < this.x + offx + width) && (mY > this.y + offy) && (mY < this.y+ offy + height));
  }
}
class GuiCustomElement extends GuiElement{
  constructor(image, type, name, x, y, gate, width=50, height=60, remaining=0, delay=0, border=2){
    super(image, type, x, y, remaining, delay);
    this.name = name;
    this.gate = gate;
    this.width = width;
    this.height = height;
    this.border = border;
    this.printingText = true;
    this.r = 2;
    this.text = this.name;
    console.log(image, type, x, y, gate);
    this.io = new Array(this.gate.ins + this.gate.outs);
    var r = this.r;
    var inSpacing = (this.height - (this.gate.ins * 2 * r))/(this.gate.ins+1);
    for (var i=0;i<this.gate.ins;++i){
      this.io[i] = {x: -this.border - r, y: inSpacing*(i+1) + r + i*2*r};
    }
    var outSpacing = (this.height - (this.gate.outs * 2 * r))/(this.gate.outs+1);
    for (var i=0;i<this.gate.outs;++i){
      this.io[i+this.gate.ins] = {x: this.width + this.border - r, y: outSpacing*(i+1) + r + i*r*2};
    }
  }
  show(){
    if (this.animation.delay>0) { this.animation.delay--; return; }
    if (this.animation.delay == 0 && this.animation.remaining > 0) { this.x++; this.animation.remaining--; }
    ctx.lineWidth = this.border;
    ctx.strokeStyle = "black";
    ctx.strokeRect(this.x-this.border, this.y-this.border, this.width + 2*this.border, this.height + 2*this.border);
    ctx.fillStyle = "gray";
    ctx.fillRect(this.x, this.y, this.width, this.height);

    var radius = this.r, x = this.x, y = this.y;
    var circles = [];
    var self = this;
    this.io.forEach(function(io){
      ctx.strokeStyle = io.color;
      ctx.lineWidth = 4;
      ctx.beginPath();
      ctx.arc(x + io.x + radius, y + io.y + radius, radius, 0, 2*Math.PI);
      circles.push({x: x + io.x + radius, y: y + io.y + radius});
      ctx.stroke();
    });
    this.circles = circles;
    if (this.printingText){
      var fontSize = 12;
      ctx.font = fontSize + "px monospace";
      ctx.fillStyle = "black";
      ctx.fillText(this.text, this.x + 3, this.y + this.height/2 + fontSize/2);
    }
  }
}

window.addEventListener('keydown', (e) => {
  switch(e.key){
    case 'ArrowUp':

    break;
    case 'ArrowDown':

    break;
    case 'ArrowLeft':

    break;
    case 'ArrowRight':

    break;
  }
});

function mouseDown(e)
{
  var i;
  var bRect = canvas.getBoundingClientRect();
  mouseX = (e.clientX - bRect.left);
  mouseY = (e.clientY - bRect.top);

  if (mouseX < 50 && mouseY < 50){
    //Call: clicking the gate menu button
    //Goal: setting gatesToSelect[]
    var index = 0;
    userGuiGates.forEach(function({image, name, height, gate}){
      console.log(index);
      var guiGate = new GuiElement(image, "menu", 15, 15, (menuGuiGates.length+userGuiGates.length-index)*50, (index)*55);
      guiGate.height = height;
      guiGate.gate = gate;
      guiGate.name = name;
      gatesToSelect.push(guiGate);
      index++;
    });
    menuGuiGates.forEach(function({image, name}){
      gatesToSelect.push(
        new GuiElement(image, name, 15, 5, (menuGuiGates.length+userGuiGates.length-index)*50, index*55)
      );
      index++;
    });

    return;
  }
  if (mouseX > 430 && mouseY < 50){
    putting_wire = 1;
    return;
  }
  if (mouseX > 355 && mouseY > 452){
    var myGate = doAssemble(cons.value, wires);
    guiGates = [];
    wires = [];
    max_in_index = -1; max_out_index = -1;
    userGates.push(myGate);

    userGuiGates.push({image: "custom", name: myGate.name, height: 30 * Math.max(myGate.ins, myGate.outs), gate: myGate});
    return;
  }
  if (putting_wire > 0){
    guiGates.forEach(function(gate){
      gate.circles.forEach(function(circle,index){
        gate.io[index].color = "#000000";
        console.log(mouseX, circle.x);
        if (((mouseX - circle.x)*(mouseX - circle.x) + (mouseY - circle.y)*(mouseY - circle.y)) < 16){
          new_wire.push({gate: gate, ioIndex: index});
        }
      });
    });
    putting_wire++;
    if (putting_wire > 2) {
      wires.push(new_wire);
      new_wire = [];
      putting_wire = 0;
    }
    return;
  }
  var quit = false;
  gatesToSelect.forEach(function(element){
    if (quit) return;
    if(element.covers(mouseX, mouseY))
    {
      drag = true;
      dragHoldX = mouseX - element.x;
      dragHoldY = mouseY - element.y;
      if (element.type === "menu"){
          guiGates.push(new GuiCustomElement("custom", "not_menu", element.name, element.x, element.y, element.gate, element.width, element.height));
      }
      else guiGates.push(new GuiElement(element.image, element.image.id, element.x, element.y));
      dragIndex = guiGates.length-1;
      gatesToSelect = [];
      quit = true;
    }
  });
  if (quit) return;
  for (i=0; i < guiGates.length; i++) {
    if(guiGates[i].covers(mouseX, mouseY))
    {
      console.log(guiGates[i]);
      drag = true;
      dragHoldX = mouseX - guiGates[i].x;
      dragHoldY = mouseY - guiGates[i].y;
      dragIndex = i;
    }
  }
  return false;
}

function mouseMove(e)
{
  if (drag){
    var posX, posY;
    var dist = 40;
    var minX = dist;
    var maxX = canvas.width - 2*dist;
    var minY = dist;
    var maxY = canvas.height - 2*dist;

    var bRect = canvas.getBoundingClientRect();
    mouseX = (e.clientX - bRect.left);
    mouseY = (e.clientY - bRect.top);

    posX = mouseX - dragHoldX;
    //posX = (posX < minX) ? minX : ((posX > maxX) ? maxX : posX);
    posY = mouseY - dragHoldY;
    //posY = (posY < minY) ? minY : ((posY > maxY) ? maxY : posY);

    guiGates[dragIndex].x = posX;
    guiGates[dragIndex].y = posY;
    return;
  }

  if (putting_wire > 0){
    var bRect = canvas.getBoundingClientRect();
    mouseX = (e.clientX - bRect.left);
    mouseY = (e.clientY - bRect.top);

    guiGates.forEach(function(gate){
      gate.circles.forEach(function(circle,index){
        gate.io[index].color = "#000000";
        if (((mouseX - circle.x)*(mouseX - circle.x) + (mouseY - circle.y)*(mouseY - circle.y)) < 16){
          gate.io[index].color = "green";
        }
      });
    });

  }


}

function mouseUp()
{
  if (drag) drag = false;
}

canvas.addEventListener("mousedown", mouseDown, false);
window.addEventListener("mousemove", mouseMove, false);
window.addEventListener("mouseup", mouseUp, false);

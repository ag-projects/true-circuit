var canvas = document.getElementById('applicationPreview');
var ctx = canvas.getContext('2d');
var cons = document.getElementById('editor');
var drag = false, mouseX, mouseY, dragHoldX, dragHoldY;
var putting_wire = 0, new_wire = [], wires = [];
var max_in_index = -1, max_out_index = -1;
var menuElements = [
  new GuiElement(add, "menu_add", 5, 5),
  new GuiElement(wire, "menu_wire", 425, 5),
  new GuiElement(assemble, "menu_assemble", 355, 430)
];

var userGates = [];

var menuGuiGates = [
  {image: out_gate, name: "menu_select_out_gate"},
  {image: in_gate, name: "menu_select_in_gate"},
  {image: and, name: "menu_select_and_gate"},
  {image: not, name: "menu_select_not_gate"}
];
var userGuiGates = [];
var gatesToSelect = []; //holds GuiElement class objects created using menuGuiGates[] and userGuiGates[] during selection


var guiGates = [];

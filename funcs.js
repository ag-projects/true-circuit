function numWithZeros(num){
  if (num<10) return "000"+num;
  if (num<100) return "00"+num;
  if (num<1000) return "0"+num;
  return num;
}
function binaryPermutation(n){
  if (n < 2){
    return [[0], [1]];
  }
  return binaryPermutation(n-1).map(a => a.concat(0)).concat(binaryPermutation(n-1).map(a => a.concat(1)));
}

function doAssemble(name, wires){
  var debug = true;
  if (debug) console.log("Assembling these:");
  if (debug) console.log(wires);
  var in_gates = 0, out_gates = 0;
  guiGates.forEach(function(gate){
    if (gate.type === "in_gate") {in_gates++; return;}
    if (gate.type === "out_gate") {out_gates++; return;}
  });

  var output = new Array(out_gates); //{args: args, ix: index} where to take output bits from (from outputs array)
  var steps = new Array (guiGates.length - in_gates - out_gates);
  var index = 0;
  guiGates.forEach(function(gate){
    if (gate.type === "in_gate" || gate.type === "out_gate") return;
    gate.id = index;
    steps[index] = {gate: gate.gate, args: new Array(gate.gate.ins), ix: new Array(gate.gate.ins)};
    index++;
  });

  wires.forEach(function(wire){ //wire:{gate: gate, ioIndex: index}
    //destination gate:wire[1].gate [wire[1].ioIndex]
    if (wire[1].gate.type === "out_gate") {
      output[wire[1].gate.ix] = {
        //output[] is meant to be used while putting together the output of function ev()
        //each output[] entry helps find a boolean value of previously evaluated gates, stored in outputs[args][ix]
        args: wire[0].gate.id, //args is a Gate index. outputs[args] should be an array with all outputs of gate no. args
        ix: wire[0].ioIndex - wire[0].gate.gate.ins   //ix is the output index that we're interested in from outputs[args]
        //subtracting wire[0].gate.ins results in getting the output index instad of input/output index
        //(if gate Z has 2 inputs and 1 output and we want to get it's value we need outputs[Z][0], not outputz[Z][2], hence the subtraction)
      };
      return;
    }
    if (wire[0].gate.type === "in_gate"){
      steps[wire[1].gate.id].args[wire[1].ioIndex] = -1;
      steps[wire[1].gate.id].ix[wire[1].ioIndex] = wire[0].gate.ix;
      return;
    }
    steps[wire[1].gate.id].args[wire[1].ioIndex] = wire[0].gate.id;
    steps[wire[1].gate.id].ix[wire[1].ioIndex] = wire[0].ioIndex - wire[0].gate.gate.ins;
  });

  //NOW SCAN steps[] for steps[n].args = [] and steps[n].args = [] (gate not connected)
  if (debug) console.log(steps);
  //(name, ins, outs, steps, output)

  return new Gate(name, in_gates, out_gates, steps, output);
  //var steps = [{gate: AND, args: [-1,-1], ix: [0,1]}, {gate: NOT, args: [0], ix: [0]}];
}

function render(){
  ctx.clearRect(0,0,canvas.width, canvas.height);
  gatesToSelect.forEach(gate => gate.show());
  guiGates.forEach(gate => gate.show());
  menuElements.forEach(function(element, index){if (index != 0) element.show();});
  wires.forEach(function(wire){
    ctx.lineWidth = 2;
    ctx.strokeStyle = "green";
    ctx.beginPath();
    ctx.moveTo(wire[0].gate.circles[wire[0].ioIndex].x, wire[0].gate.circles[wire[0].ioIndex].y);
    ctx.lineTo(wire[1].gate.circles[wire[1].ioIndex].x, wire[1].gate.circles[wire[1].ioIndex].y);
    ctx.stroke();
  });
  menuElements[0].show();
  window.requestAnimationFrame(render);
}
